package com.app.mapper;

import com.app.entity.User;

public interface UserMapper {

	User findUserById(Integer id);
	
	public Integer saveUser(User user);
}
