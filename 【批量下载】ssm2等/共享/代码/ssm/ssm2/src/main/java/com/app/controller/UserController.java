package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.entity.User;
import com.app.servcie.UserService;

@Controller
public class UserController {
	@Autowired
	private UserService userService;

	@RequestMapping("/insertUser")
	@ResponseBody
	public User insertUser(User user) {
		userService.saveUser(user);
		User resultUser = userService.findUserById(user.getId());		
		return resultUser;
	}
}
