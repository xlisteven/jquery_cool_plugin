package com.app.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import com.app.entity.User;

public interface UserMapper {

	@Select("select * from tb_user where id = #{id}")
	User findUserById(Integer id);

	@Insert("insert into tb_user(userName, birth) values(#{userName}, #{birth})")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	public Integer saveUser(User user);
}
