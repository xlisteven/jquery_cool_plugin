package com.app.servcie;

import com.app.entity.User;

public interface UserService {

	User findUserById(Integer id);

	Integer saveUser(User user);
}
